%% Optimization
%
%
%% Nelder-Mead Example
%
% Define the following funcitons: 

addpath('~/toolbox/compecon/')
clear

%%
% Banana function:
f = @(x) -100*(x(2)-x(1)^2)^2-(1-x(1))^2;

%%
% Now call the COMPECON Max routine for nelder-mead

tic
x = neldmead(f,[1;0])
toc

%%
% Now call the MATALB MIN routine for Nelder-Mead for the banana function

options= optimset('Display','iter','MaxIter',100);
tic
y = fminsearch(@(x) -f(x),[1;0],options)
toc

%% Newton Raphson Method
%
% * Use successuve quadtratic approximations to the objective
% * Hope is that the max of the apporximants converges to the max of the
% objective. 
% * This is the EXACT same principle as the Newton root-fnding routine. 
% * Just apply Newton's for root-finding to the gradient.

%%
% Iterative procedure:

%%
% * provide guess $x^{(0)}$
% * Maximize the 2nd order Taylor expansion to $f$ about $x^{(0)}$:

%%
% $f(x) \approx f(x^{(0)} + f'(x^{(0)}(x - x^{(0)}) +
% \frac{1}{2}(x-x^{(0)})^Tf''(x^{(0)}(x - x^{(0)})$

%%
% NOTE that if you solve the first order condition then we have a root finidng
% problem! 

%%
% This yields the iterative rule: $x^{(1)} \leftarrow x^{(0)} - [f''(x^{(0)}]^{-1}f'(x^{(0)})$

%%
% *RECALL* the iterative rule for Newton's Method: $x^{t+1}\leftarrow x^t - [f'(x^t)]^{-1}f(x^t)$

%%
% This is the same iterative rule for rootfinding, expcet "one-level" up. 

%%
% In theory this method will converge to a _local max_ if $f$ is twice continuously
% differentiable, and if the initial guess is "sufficiently close to a local
% max, at which $f''$ is negative definite. In practice the Hessian must be well
% conditioned, ie we do not want to divide by zero. 

%%
% In practice this method is not used because

%%
% * Must compute both first and second derivatives
% * No guarantee that next step increases the function value because $f''$ need
% not satidfy the Second Order Conditions (negative definiteness).
% * Also, like most of the procedures we will talk about, we can only find local
% min

%% Quasi-Newton Methods
%
% In practice, we use a strategy similar to Newton-Raphson, but employ an
% approximation to the Hessian that we force/require to be negative-definite.
%
% This guarantees that the function can be increased in the direction of the
% Newton Step
%
% In practice, we will use solvers that approximate the inverse of the Hessian,
% and do not require any information about the true Hessian.

%%
% We will use the following update rule, analogous to Newton-Raphson:

%%
% $d^{(k)} = -B^{(k)}f'(x^{(k)})$

%%
% where $B^{(k)}$ is the approximation to the inverse of the Hessian. 

%% 
% QN methods differ in how they update the inverse Hessian:

%%
% *Steepest Ascent:* 
%
% set $B^{(k)}=-I$

%%
% *DFP*
%
% $B \leftarrow B + \frac{dd^T}{d^Tu} - \frac{Buu^TB}{u^TBu}$
%
% where $d = x^{(k+1)} - x^{(k)}$ and $u = f'(x^{(k+1)}) - f'(x^{(k)})$

%%
% *BFGS*
%
% $B \leftarrow B + \frac{1}{d^Tu}(wd^T + dw^T - \frac{w^Tu}{d^Tu}dd^T)$
%
% where $w = d - Bu$

%% Examples
% Compare Q-N method to Nelder-Mead method above.

%%
% *Banana funciton (without supplied derivative)*

options = optimoptions('fminunc','Display','iter','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs')

tic
xnew = fminunc(@(x) -f(x),[1;0],options)
toc

%%
% *Banana funciton (with supplied derivative)*
%
% In the previous class this did not converge -- WHY? Because I coded up the
% derivative wrong. 

options = optimoptions('fminunc','Display','iter','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs')

tic
xnew = fminunc('banana',[1;0],options)
toc

%%
% Banana function file:

%%
% <include>banana.m</include>




%% Maximum Likelihood
%
% Special case where we know the form of the Hessian. Basic idea: choose a
% distribution funciton for some data, $y$, that depends on unknown parameters,
% $\theta$. 
%
% The log likelihood function is the sum of the logs of the likelihoods of each
% of the data observations: $l(\theta; y) = \sum_n ln(f(y_i;\theta))$
%
% Define the "score" function as the matrix of derivatives of the LL fucntion
% evaluated at each observation: $s_i(\theta;y) = \frac{\partial l(\theta; y_i) }{\partial \theta}$
%
% Now consider the $n\times k$ score matrix. The expectation of the inner product
% of the score function is equal to the negative of the expectation of the
% second derivative of the likelihood function ("information" matrix). This is a
% positive definite that we can use in place of the Hessian to update the search
% direction. 
%
% $d = -[s(\theta;y)^Ts(\theta;y)]^{-1}s(\theta;y)^T1_n$
%
% And the inverse "Hessian" is an estimate for the covariance of $\theta$.

%% "Global Optimization"
%
% * Simulated Annealing
% * Genetic Algorithm 
% * Pattern Search
% * MCMC approaches: Chernozhukov and Hong (2003)
%
% These methods can be very very slow to converge, but are useful in cases where
% you know your objective function is non-smooth or very ill-behaved. 






clear;
%% Question 1
X = [1, 1.5, 3.5, 4.78, 5, 7.2, 9, 10, 10.1, 10.56];
Y1 = -2 + .5*X;
Y2 = -2 + .5*X.^2;

plot(X,Y1,'--',X,Y2,'-');
legend('Y1','Y2');

%% Question 2
X = -20:(40+20)/119:40;
disp('Sum of vector elements is');
sprintf('%13.25f',sum(X))

% note numerical error of summation of small numbers.

%% Question 3
A = [3 4 5; 2 12 6; 1 7 4];
b = [3; -2; 10];
C = A'*b
D = (A'*A)\b
E = sum(b'*A)
F = A([1,3],1:2)
x = A\b

%% Question 4
B = kron(eye(5),A)

%% Question 5
A = random('norm',6.5,3,5,3)
B = A>=4
% note that matlab random number generator parameters are mean and standard
% deviation. If you generate standard normal and do linear transformation,
% you should multiply on sqrt(3).
%% Question 6
% import data
dat = readtable('datahw1.csv','ReadVariableNames',false);
M = csvread('datahw1.csv');
% give variables names
dat.Properties.VariableNames = {'firm_id','year','Export','RD','prod','cap'};
% create deletion index to remove observations with NaNs
del_ind = sum(isnan(dat{:,:}),2);
% delete rows with NaNs 
dat(del_ind>0,:) = [];

% estimate linear model by matlab econometrics toolbox
lm1 = fitlm(dat,'prod~Export+RD+cap');
% display all the statistics of estimation 
lm1

% or display only coefficients
disp('Beta');
disp(lm1.Coefficients.Estimate);

% or use old-school estimation method
X = [ones(height(dat),1) dat.Export dat.RD dat.cap];
b = (X'*X)\X'*dat.prod

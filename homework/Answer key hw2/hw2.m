clear all;
%% setting up parameter values
v = [-1; -1; -1];
p = [1; 1; 1];
%% Question 1
q(p,v)


%% Question 2
options = optimset('Display','off','Algorithm', 'trust-region-dogleg');

[x, fval, ~, it] = broyden(@(x) foc(x,v),[1;1;1]);

% if you dont want to use broyden, take a look at fsolve
% [x, fval, ~, it] = fsolve(@(x) foc(x,v),[1;1;1]);

disp('Started at');
disp([1 1 1]);
disp('potential solution');
disp(x);
disp('Exit excess at FOC');
disp(fval);
disp('Number of iterations');
disp(it);

[x, fval, ~, it] = broyden(@(x) foc(x,v),[0;0;0]);
disp('Started at');
disp([0 0 0]);
disp('potential solution');
disp(x);
disp('Exit excess at FOC');
disp(fval);
disp('Number of iterations');
disp(it);

[x, fval, ~, it] = broyden(@(x) foc(x,v),[0;1;2]);
disp('Started at');
disp([0 1 2]);
disp('potential solution');
disp(x);
disp('Exit excess at FOC');
disp(fval);
disp('Number of iterations');
disp(it);

[x, fval, ~, it] = broyden(@(x) foc(x,v),[3;2;1]);
disp('Started at');
disp([3 2 1]);
disp('potential solution');
disp(x);
disp('Exit excess at FOC');
disp(fval);
disp('Number of iterations');
disp(it);
%% Question 3
iter = 1;
maxiter = 100;
err = 1;
eps = 1e-12;
p=[1;1;1]
while (iter<maxiter && err>eps)
    for i=1:length(p)
        options = optimset('MaxIter',1,'Display','off');
        pstatic = p;
        % here I am using solution of equation instead of secant method.
        % It should be a little faster, but not a whole lot
        % make only 1 iteration, solving only one equation at a time
        guess = p(i);
        p(i) = fsolve(@(x) foc(x,v,i,pstatic),guess,options);
    end
    err = sum(abs(foc(p,v)));
    iter = iter+1;
end
disp('started from')
disp([ 1 1 1]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);



iter = 1;
maxiter = 100;
err = 1;
eps = 1e-12;
p=[0;0;0];
while (iter<maxiter && err>eps)
    for i=1:length(p)
        options = optimset('MaxIter',1,'Display','off');
        % make only 1 iteration, solving only one equation at a time
        p = fsolve(@(x) foc(x,v,i),p,options);
    end
    err = sum(abs(foc(p,v)));
    iter = iter+1;
end
disp('started from')
disp([ 0 0 0]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);


iter = 1;
maxiter = 100;
err = 1;
eps = 1e-12;
p=[0;1;2];
while (iter<maxiter && err>eps)
    for i=1:length(p)
        options = optimset('MaxIter',1,'Display','off');
        % make only 1 iteration, solving only one equation at a time
        p = fsolve(@(x) foc(x,v,i),p,options);
    end
    err = sum(abs(foc(p,v)));
    iter = iter+1;
end
disp('started from')
disp([ 0 1 2]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);


iter = 1;
maxiter = 100;
err = 1;
eps = 1e-12;
p=[3;2;1];
while (iter<maxiter && err>eps)
    for i=1:length(p)
        options = optimset('MaxIter',1,'Display','off');
        % make only 1 iteration, solving only one equation at a time
        p = fsolve(@(x) foc(x,v,i),p,options);
    end
    err = sum(abs(foc(p,v)));
    iter = iter+1;
end
disp('started from')
disp([ 3 2 1]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);

%% Question 4
err = 1;
iter = 1;
itermax = 100;
p = [1;1;1];
while (err>eps && iter < itermax)
    iter = iter+1;
    Q = q(p,v);
    p_t = 1./(1-Q(2:end));
    err = sum(abs(p-p_t));
    p = p_t;
end
disp('started from')
disp([ 1 1 1]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);

err = 1;
iter = 1;
itermax = 100;
p = [0;0;0];
while (err>eps && iter < itermax)
    iter = iter+1;
    Q = q(p,v);
    p_t = 1./(1-Q(2:end));
    err = sum(abs(p-p_t));
    p = p_t;
end
disp('started from')
disp([ 0 0 0]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);

err = 1;
iter = 1;
itermax = 100;
p = [0;1;2];
while (err>eps && iter < itermax)
    iter = iter+1;
    Q = q(p,v);
    p_t = 1./(1-Q(2:end));
    err = sum(abs(p-p_t));
    p = p_t;
end
disp('started from')
disp([ 0 1 2]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);

err = 1;
iter = 1;
itermax = 100;
p = [3;2;1];
while (err>eps && iter < itermax)
    iter = iter+1;
    Q = q(p,v);
    p_t = 1./(1-Q(2:end));
    err = sum(abs(p-p_t));
    p = p_t;
end
disp('started from')
disp([ 3 2 1]);
disp('final error');
disp(err);
disp('number of iterations');
disp(iter);
disp('final point');
disp(p);

%% question 5
% create matrix of q
V = -1*ones(3,6);
V(3,:)=-4:1;
P = zeros(3,6);
for i=1:6
     P(:,i) = broyden(@(x) foc(x,V(:,i)),[1;1;1]);
end

plot(V(3,:)',P(1,:)',V(3,:)',P(3,:)');
xlabel('V_3');
ylabel('P');
legend('P_1','P_3');

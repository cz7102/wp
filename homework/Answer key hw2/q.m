function q = q(p,v)
% Checking that sizes of price and qulity correpond
    if length(p)~=length(v)
        disp('size of price vector does not correspond to size of v vector');
    end
% Extending price and quality with outcide good parameters
    P = [0; p];
    V = [0;v];
    
% calculazting denominator (not necessary)
    denom = sum(exp(V-P));
% calculating market shares of each product
    q = exp(V-P)/denom;
end
function foc = foc(p,v,n,pstatic)
% this function is supposed to calculate vector of excesses in FOC of each
% firm's maximization problem

if nargin>3
   p_use = pstatic;
   p_use(n) = p;
   p = p_use;
end


% calculate market shares given parameters
    Q = q(p,v);
% now throw away outside good market share
    Q = Q(2:end);
    
    
    
    foc_t = p - 1./(1-Q);
%     foc_t = (1-p).*Q + p.*Q.^2;
    
%     These two above FOCs are equivalent mathematically when Q>0, but the
%     first one gives you much netter numerical properties because of the
%     lack of singularity. Homework did not specify which one to use,
%     though
    
% optional argument n
    if nargin==2
        foc = foc_t;
    elseif nargin >2
        foc = foc_t(n);
    end
end
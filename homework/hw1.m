%% Empirical Method, Homework1
%% Name: Chen Zhang

clear,clc;
%%%%%%%%%%%%%%%%%%%%%%
%% Question 1
%%%%%%%%%%%%%%%%%%%%%%
display('##Question 1');
X  = [1, 1.5, 3, 4, 5, 7, 9, 10];
Y1 = -2 + 0.5*X;
Y2 = -2 + 0.5*X.^2;
plot(X,Y1);
hold on
plot(X,Y2);
xlabel('X');
ylabel('Y');


%%%%%%%%%%%%%%%%%%%%%%
%% Question 2
%%%%%%%%%%%%%%%%%%%%%%
display('##Question 2')
X = linspace(-10,20,200);
sum(X)

%%%%%%%%%%%%%%%%%%%%%%
%% Question 3
%%%%%%%%%%%%%%%%%%%%%%
display('##Question 3')
A = [2,4,6; 1,7,5; 3,12,4];
B = [-2,3,10]';
C = A'*B
% inv is slow. see the answer key to see a good alternative
D = inv(A'*A)*B
E = sum(A'*B)
F = A([1 3],1:2)
x = inv(A)*B

%%%%%%%%%%%%%%%%%%%%%%
%% Question 4
%%%%%%%%%%%%%%%%%%%%%%
display('##Question 4')
B = kron(eye(5),A)

%%%%%%%%%%%%%%%%%%%%%%
%% Question 5
%%%%%%%%%%%%%%%%%%%%%%
display('##Question 5')
rng(7102)
mu = 10;
sigma = 5;
A = mu + randn(5,3)*sigma
A_2 = A>=10

%%%%%%%%%%%%%%%%%%%%%%
%% Question 6
%%%%%%%%%%%%%%%%%%%%%%
load datahw1.csv

id = datahw1(:,1);
year = datahw1(:,2);
ex = datahw1(:,3);
rd = datahw1(:,4);
prod = datahw1(:,5);
cap = datahw1(:,6);
X = [ex rd cap];
N = length(prod);
X = [ones(N,1) X];

% again, inv is slow. see the answer key
b = inv(X'*X)*X'*prod
cov = inv(X'*X)

% try to do the heteroscedasticity robust standard errors
se = diag(sqrt(cov))
t = sqrt(N)*b./se
pvalue = (1-tcdf(t,N-1))*2




















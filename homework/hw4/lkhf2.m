function L=lkhf2(para,k,node,w)
global N T X Y Z

mu = para(1:2);
gamma = para(3);

sigma = [para(4),para(5);para(5),para(6)];
[R p] = chol(sigma);
if p>0
    L=-10^10;
end
    


node = ones(k,1)*mu'+node*R;

lkhf = ones(k,N);
for i=1:N
    for t=1:T
        y1 = 1./(1+exp(-node(:,1).*X(t,i)-gamma*Z(t,i)-node(:,2))).^Y(t,i);
        y0 = (1-1./(1+exp(-node(:,1).*X(t,i)-gamma*Z(t,i)-node(:,2)))).^(1-Y(t,i));
        lkhf(:,i) = lkhf(:,i).*y1.*y0;
    end
end

L = 1;
int_lkhf=zeros(N,1);
for i=1:N
    int_lkhf(i)=log(lkhf(:,i)'*w);
    L = int_lkhf(i)+L;
end
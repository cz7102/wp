function L=lkhf1(para,k,node,w)
global N T X Y Z

b0 = para(1);
gamma = para(2);
sigma_b = para(3);

node_b = b0+node(:,1).*sqrt(sigma_b);

lkhf = ones(k,N);
for i=1:N
    for t=1:T
        y1 = 1./(1+exp(-node_b.*X(t,i)-gamma*Z(t,i))).^Y(t,i);
        y0 = (1-1./(1+exp(-node_b.*X(t,i)-gamma*Z(t,i)))).^(1-Y(t,i));
        lkhf(:,i) = lkhf(:,i).*y1.*y0;
    end
end

L = 1;
int_lkhf=zeros(N,1);
for i=1:N
    int_lkhf(i)=log(lkhf(:,i)'*w);
    L = int_lkhf(i)+L;
end
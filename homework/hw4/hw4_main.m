clear, clc;

% lkhf1.m is log-likelihood function used for solving q1-q3
% lkhf2.m is log-likelihood function used for solving q4
load hw4data
global N T X Y Z

N = data.N;
T = data.T;
X = data.X;
Y = data.Y;
Z = data.Z;

b0 = 0.1;
gamma = 0;
sigma_b = 1;
para1 = [b0,gamma,sigma_b]';

%% q1
% k1, x1 and w1 are number of nodes, GQ nodes and weights.
k1 = 20;
[x1,w1] = qnwnorm(k1,b0,sigma_b);

display('Question 1, log-likelihood is')
L1 = lkhf1(para1,k1,x1,w1)

%% q2
% k2 and x2 are number of nodes and Monte Carlo nodes.
% weights are just 1/k2
k2 = 100;
x2 = randn([k2,1]);
w2 = ones(k2,1)/k2;

display('Question 2, log-likelihood is')
L2 = lkhf1(para1,k2,x2,w2)

%% q3
display('Question 3')
% para is the parameter [beta gamma sigma11]
% Gaussian Quadrature
lkhf_gq = @(para) -lkhf1(para,k1,x1,w1);

display('GQ estimate is')
[para_gq fval] = fminsearch(lkhf_gq,para1)

% Monte Carlo
lkhf_MC = @(para) -lkhf1(para,k2,x2,w2);
display('MC estimate is')
[para_MC fval] = fminsearch(lkhf_MC,[0.2,0.1,0.9])


%% q4
u0 = 0.1;
para2 = [b0,u0,gamma,sigma_b,0.1,1]';

% k4, x4 and w4 are defined as in q2.
k4 = 100;
x4 = randn([k4,2]);
w4 = ones(k4,1)/k4;

% set some lower bound and upper bound for parameters
lb = [-2 -2 -2 0.01 -1 0.01];
ub = [2 2 2 1 1 1];

% these bounds are unnecessary, theory does not give you those

lkhf2_MC = @(para) -lkhf2(para,k4,x4,w4);
display('Question 4, estimate is')
[para2_MC,fval] = fmincon(lkhf2_MC,para2,[],[],[],[],lb,ub)

% two of your parameters did not change because of your boundaries.












function bertrand = bertrand(p)
v = -1*ones(3,1);
d = 1+ones(1,3)*exp(v-p);
q = exp(v-p)/d;
bertrand = -(1-q).*p+ones(3,1);
end
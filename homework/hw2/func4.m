function [p,it] = func4(p)
maxit     = optget('part_4','maxit',100);
tol       = optget('part_4','tol',sqrt(eps));

v = -1*ones(3,1);
d = 1+ones(1,3)*exp(v-p);
q = exp(v-p)/d;
p = 1./(1-q);
fval = feval('bertrand',p);
fnorm = norm(fval,inf);
for it =1:maxit
    if fnorm<tol, return;end
    d = 1+ones(1,3)*exp(v-p);
    q = exp(v-p)/d;
    p = 1./(1-q);
    fval = feval('bertrand',p);
    fnorm = norm(fval,inf);
end
end
function [x1,it] = func5(f,x)

maxit     = optget('gaujac','maxit',100);
tol       = optget('gaujac','tol',sqrt(eps)); 

n = length(x);
fval = feval(f,x);
f_deriv = diag(fdjac(f,x));
x0 = x;
x1 = x0 - fval./f_deriv;
xx = x1;
f0 = feval(f,x0);
f1 = feval(f,x1);
fnorm = norm(f1,inf);

for it = 1:maxit
    if fnorm<tol, return;end
    f_deriv = (f1-f0)./(x1-x0);
    xx = x1 - f1.*f_deriv;  % single step
    x0 = x1; x1 = xx;      % update
    ff = feval(f,x1);
    f0 = f1; f1 = ff;
    fnorm = norm(f1,inf);
end
end


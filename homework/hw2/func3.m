function [x1,it] = func3(f,x)

maxit     = optget('gaujac','maxit',100);
tol       = optget('gaujac','tol',sqrt(eps)); 

n = length(x);
fval = feval(f,x);
f_deriv = diag(fdjac(f,x));
x0 = x;
x1 = x0 - fval./f_deriv;
f0 = feval(f,x0);
f1 = feval(f,x1);
fnorm = norm(f1,inf);

for it = 1:maxit
    if fnorm<tol, return;end
    for i = 1:n
        xx0 = x0; % these four are temp values in sub-iteration
        xx1 = x1;
        ff0 = f0;
        ff1 = f1;
        for j = 1:maxit
            if abs(ff1(i))<tol, break;end
            xx(i) = xx1(i) - ff1(i)*(xx1(i)-xx0(i))/(ff1(i)-ff0(i));
            xx0(i) = xx1(i);xx1(i) = xx(i);
            ff = feval(f,xx1);
            ff0 = ff1;ff1=ff;
        end
        x(i) = xx1(i);
    end
    x0=x1;x1=x;
    f1 = feval('bertrand',x1);
    fnorm = norm(f1,inf);
end

end


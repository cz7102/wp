clear,clc
%% question 1
v = -1*ones(3,1);
p = ones(3,1);
d = 1+ones(1,3)*exp(v-p);
q = exp(v-p)/d
q0 = 1-sum(q)

%% question 2
% Solve foc system
% $-(1-q_j)*p_j+1 = 0, for all j$
%-------------------------------------
tic;[p2_a,~,~,it] = broyden('bertrand',[1,1,1]')
toc
tic;[p2_b,~,~,it] = broyden('bertrand',[0,0,0]')
toc
tic;[p2_c,~,~,it] = broyden('bertrand',[0,1,2]')
toc
tic;[p2_d,~,~,it] = broyden('bertrand',[3,2,1]')
toc

%% question 3
tic;[p3_a,it] = func3('bertrand',[1,1,1]')
toc
tic;[p3_b,it] = func3('bertrand',[0,0,0]')
toc
tic;[p3_c,it] = func3('bertrand',[0,1,2]')
toc
tic;[p3_d,it] = func3('bertrand',[3,2,1]')
toc

%% question 4
tic;[p4_a,it] = func4([1,1,1]')
toc
tic;[p4_b,it] = func4([0,0,0]')
toc
tic;[p4_c,it] = func4([0,1,2]')
toc
tic;[p4_d,it] = func4([3,2,1]')
toc

%% question 5
tic;[p5_a,it] = func5('bertrand',[1,1,1]')
toc
tic;[p5_b,it] = func5('bertrand',[0,0,0]')
toc
tic;[p5_a,it] = func5('bertrand',[0,1,2]')
toc
tic;[p5_b,it] = func5('bertrand',[3,2,1]')
toc








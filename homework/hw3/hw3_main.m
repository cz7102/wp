clear, clc
load hw3.mat


%% ----- Part A -------------------------------
% clear, clc;

% (a) The focs are
%     (1)  -psi(theta_1)+log(theta_2)+mean(log(x))=0
%     (2)  -theta_1/theta_2 = Y_1


% (b) From (2) theta_2 = theta_1/Y_1
%     plug this into (1), we get
%     -psi(theta_1)+log(theta_1)+log(Y_2/Y_1)=0
%

% (c) Suppose x is a vevctor of observations

% Y_1 = mean(x);
% Y_2 = exp(mean(log(x)));
% see function qc_mle.m

% (d) we have 
%      -psi(theta_1)+log(theta_1)+log(Y_2/Y_1)=0
%      theta_1 only depends on Y_2/Y_1, solve this nonlinear equation

y = 1.1:0.01:3;
theta = zeros(1,length(y));
for i=1:length(y)
    f = @(theta_1) -psi(max(theta_1,0))+log(theta_1)-log(y(i));
    theta(i) = broyden(f,4);
    % you were not supposed to use any general optimization techniques
    i
end
plot(theta(1,:),y);







%% ----- Part B -------------------------------
% q1
b0 = zeros(6,1);
f = @(beta)loglkh(y,X,beta);
g = @(beta)(-loglkh(y,X,beta));

% this function presumes that variable X is in memory, but at the time of
% execution there is no variable X in memory. It is deleted by line 6 which
% I needed to comment to run. please make sure your program runs before
% submitting.

% fminunc
options = optimoptions('fminunc','SpecifyObjectiveGradient',false);
[beta_1,fval_1,exitflag,output_1] = fminunc(g,b0);

% your function does not retutn scalar value, it does not work.

options = optimoptions('fminunc','SpecifyObjectiveGradient',true);
[beta_2,fval_2,exitflag,output_2] = fminunc(g,b0);

% nelder mead
optset('neldmead','maxit',2000);
beta_nm = neldmead(f,b0);

% bhhh MLE
[beta_bhhh Q H_bhhh H_init iter_bhhh] = bhhh(y,X,b0,0.001,0.9999);

% q2 eigenvalue
eig1 = eig(inv(H_init)); % initial Hessian
eig2 = eig(inv(H_bhhh)); % bhhh Hessian

% q3
[beta_nls H_nls iter_nls] = nls(y,X,b0);

% q4
se_bhhh = diag(H_bhhh).^0.5;
se_nls = diag(H_nls).^0.5;






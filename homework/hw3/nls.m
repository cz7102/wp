function [bb H it]= nls(y,X,b0)

tol=10^-6;
N = length(y);
bb = b0;
b = b0-1;
H = zeros(6);
Q = 0;
q = zeros(6,1);
it = 1;

while abs(max(bb-b))>tol
    beta_temp = bb;
    Q = 0;H=0;q=0;
    for i=1:N
        qi =-2*(y(i)-exp(X(i,:)*beta_temp))*X(i,:)';
        H = H + qi*qi'/N;
        q = q + qi/N;
        Q = Q + (y(i)-exp(X(i,:)*beta_temp))^2/N;
    end
    d = -H\q*Q;
    b = beta_temp;
    bb = beta_temp + d;
    it = it+1;
end





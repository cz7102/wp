function tt = qc_mle(Y_1,Y_2)

% Q = -log(Gamma(x))+theta_1*log(theta_2)+(theta_1-1)*log(Y_2)-theta_2*Y_1;
% q = [-psi(theta_1)+log(theta_2)+log(Y_2);
%      theta_1/theta_2-Y_1];  % first order derivative of obj function Q
%  
% H = [-trigamma(theta_1), 1/theta_2;
%      1/theta_2, -theta_1/theta_2];  % Hessian of Q

 tol = 10^-4;
 theta = [0;0];
 tt = [1;1];
 
 while max(abs(tt-theta))>tol
     temp = max(tt,[0.01;0.01]);
     q = [-psi(temp(1))+log(temp(2))+log(Y_2);
          temp(1)/temp(2)-Y_1];
     H = [-trigamma(temp(1)), 1/temp(2);
          1/temp(2), -temp(1)/temp(2)];
     tt = temp -H\q;
     theta = temp;
 end
 
end

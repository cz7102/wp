function [bb QQ H H_init it] = bhhh(y,X,b0,u0,u1)

tol=10^-6;
N = length(y);
bb = b0;
b = b0-1;
H = zeros(6);
QQ = 1;
Q_temp = 0;
it = 1;
while max(abs(bb-b))>tol
    s = 1;
    beta_temp = bb;
    H = 0;
    q = zeros(6,1);
    for i=1:N
        qi =(y(i)-exp(X(i,:)*beta_temp)).*X(i,:)';
        H = H + qi*qi'/N;
        q = q + qi/N;
    end
    if beta_temp==bb
        H_init=H;
    end
    
    d = H\q;
    Q = @(s)sum(-exp(X*(beta_temp + s*d))+y.*(X*(beta_temp + s*d)));
    if feval(Q,1)-feval(Q,0)<u0*q'*d
        s = golden(Q,0,2);
    end
    b = beta_temp;
    bb = beta_temp + s*d;
    Q_temp = feval(Q,0);
    QQ = feval(Q,s);
    it = it+1;
end

end

    
    
    
    
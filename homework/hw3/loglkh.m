function Q=loglkh(y,x,beta)

lambda = exp(x*beta);
Q = -lambda+y.*log(lambda);
Q = sum(Q);

end